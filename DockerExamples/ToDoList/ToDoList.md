To Start the application use the following 4 commands:

docker network create todonet

docker run --name postgresdb --network todonet -p 5432:5432 -e POSTGRES_PASSWORD=password -e POSTGRES_USER=matthias -e POSTGRES_DB=mydb -d postgres:latest

docker run -d -p 8080:8080 --network todonet -e SPRING_PROFILES_ACTIVE=prod -e POSTGRES_HOST=postgresdb --name todobackend cvchaths/todobackend:v0.1

docker run -d --network todonet -p 8090:8090 --name todoui cvchaths/todoui:v0.1
