package de.trloit00.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/testGetApi")
	public String testGetApi(){
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/testGetMapping/{id}")
	@ResponseBody
	public String testGetMappingById(@PathVariable String id) {
		return "ID: " + id;
	}
}