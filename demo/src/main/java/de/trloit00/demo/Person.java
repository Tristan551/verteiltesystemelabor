package de.trloit00.demo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {
    @Id
    private String name;
    private String town;

    public Person(){
        this.name = "DefaultName";
        this.town = "DefaultTown";
    }

    public Person (String name, String town){
        this.name = name;
        this.town = town;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getTown(){
        return town;
    }

    public void setTown(String town){
        this.town = town;
    }
}
