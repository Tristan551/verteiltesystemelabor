package de.trloit00.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    @Autowired
	private PersonRepository personRepository;

	@GetMapping("/person")
	@ResponseBody
	public Iterable<Person> getPersons(){
		return personRepository.findAll();
	}

	@GetMapping("/person/{name}")
	@ResponseBody
	public Person getPerson(@PathVariable String name) {
		return personRepository.findById(name).get();
	}

	@PostMapping("/person/{name}")
	public void createPerson(@PathVariable String name, @RequestParam String town) {
		personRepository.save(new Person(name, town));
	}

	@DeleteMapping("/person/{name}")
	public void deletePerson(@PathVariable String name){
		personRepository.deleteById(name);
	}

	@PutMapping("person/{name}")
	public void changePerson(@PathVariable String name, @RequestParam String town)
	{
		var person = personRepository.findById(name).get();
		person.setTown(town);
		personRepository.save(person);
	}
}