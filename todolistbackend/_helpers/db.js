const mongoose = require('mongoose');
const connectionOptions = { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false };
//connect to the database which is specified in the config.json file
mongoose.connect("mongodb://mongo:27017/todolist", connectionOptions);
mongoose.Promise = global.Promise;

module.exports = {
    todolist: require('../todolist.model')
};