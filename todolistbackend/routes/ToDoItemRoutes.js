'use strict';

const Router = require('express');
const todolistService = require('../todolist.service');

const getToDoItemRoutes = app => {
  const router = new Router();

  router
    .get('/:name', (req, res) => {
      todolistService.get(req.params.name).then(val => res.send(val));
    })
    .get('/', (req, res) => {
      todolistService.getAll().then(val => res.send(val));
    })
    .delete('/:name', (req, res) => {
      todolistService._delete(req.params.name).then(val => res.send(val));
    })
    .post('/', (req, res) => {
      todolistService.create(req.body).then(val => res.send(val));
    })
    .put('/', (req, res) => {
      todolistService.update(req.body).then(val => res.send(val));
    });

  app.use('/todos', router);
};

module.exports = getToDoItemRoutes;
