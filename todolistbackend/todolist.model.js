const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//schema to store users and status
const schema = new Schema({
    todo: { type: String, unique: true, required: true },
    priority: { type: Number, required: true }
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.id;
        delete ret.hash;
    }
});

module.exports = mongoose.model('todolist', schema);