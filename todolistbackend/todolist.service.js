const db = require('./_helpers/db');
const todolist = db.todolist;

module.exports = {
    getAll,
    create,
    get,
    _delete,
    update
};

// This function returns all items from the db
async function getAll() {
    return await todolist.find();
}

async function get(name) {
    const item = await todolist.findOne({ todo: name });
    return item ? item : "Task not found!";
}

async function _delete(name) {
    const item = await todolist.findOne({ todo: name });

    if (!item)
        return "Task not found!"

    item.remove();

    return "Task sucessfully removed!";
}

async function create(task) {
    // validate
    if (!task.todo)
        return "To Do Missing!"

    if (isNaN(Number(task.priority)))
        return "Priority needs to be a valid number!"

    if (await todolist.findOne({ todo: task.todo })) {
        return "Task already exists!";
    }

    const todolistitem = new todolist(task);

    await todolistitem.save();

    return todolistitem;
}

async function update(task) {
    // validate
    if (!task.todo)
        return "To Do Missing!"

    if (isNaN(Number(task.priority)))
        return "Priority needs to be a valid number!"

    let todolistitem = await todolist.findOne({ todo: task.todo });

    if (!todolistitem) {
        return "Task doesn't exist, so it can't be updated!";
    }

    todolistitem.priority = Number(task.priority);

    await todolistitem.save();

    return todolistitem;
}
